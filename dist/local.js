/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
// Setup Express for Google Cloud Functions
const server_1 = require("./server");
const server = server_1.default();
// Start local express app (for debug purpose)
const port = Number(process.env.PORT) || 9000;
server.listen(port, () => console.log(`🚀  Server ready at port ${port}`));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvbG9jYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7O0lBUUk7QUFDSixZQUFZLENBQUE7O0FBRVosMkNBQTJDO0FBQzNDLHFDQUFrQztBQUVsQyxNQUFNLE1BQU0sR0FBRyxnQkFBVyxFQUFFLENBQUE7QUFFNUIsOENBQThDO0FBQzlDLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQTtBQUU3QyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUEifQ==