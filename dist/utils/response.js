/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const RESPONSE_ALLOWED_HEADERS = 'Content-Type, Content-Length, Accept, Origin, ETag, Authorization, X-Requested-With, X-CSRF-Token, X-Api-Key';
// Callback response helper
exports.sendResponse = (response, body, statusCode = 200, extraHeaders = {}, cookie = undefined) => {
    // Get header keys
    let extraHeaderKeys = Object.keys(extraHeaders).join(", ");
    // Add security header
    extraHeaderKeys = extraHeaderKeys + (extraHeaderKeys.length > 0 ? ", " : "") + "Authorization";
    // Add cookies
    if (cookie !== undefined)
        extraHeaders = Object.assign({}, extraHeaders, { "Access-Control-Expose-Headers": extraHeaderKeys });
    const headers = Object.assign({ "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Credentials": true }, extraHeaders);
    response.set(headers).status(statusCode).send(body).end();
};
// Error response helper
exports.errorResponse = (response, body, statusCode = 500) => exports.sendResponse(response, { "code": "Error", "message": body }, statusCode);
// CORS preflight response helper
exports.preflightResponse = (response) => response
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Methods', 'GET, POST')
    .set('Access-Control-Allow-Headers', RESPONSE_ALLOWED_HEADERS)
    .set('Access-Control-Max-Age', `${1 * 60 * 60}`) // 1 hour
    .status(204).send().end();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvdXRpbHMvcmVzcG9uc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7O0lBUUk7QUFDSixZQUFZLENBQUE7O0FBS1osTUFBTSx3QkFBd0IsR0FBRyw4R0FBOEcsQ0FBQTtBQUUvSSwyQkFBMkI7QUFDZCxRQUFBLFlBQVksR0FBRyxDQUFDLFFBQWtCLEVBQUUsSUFBaUMsRUFBRSxhQUFxQixHQUFHLEVBQUUsZUFBdUIsRUFBRSxFQUFFLFNBQTZCLFNBQVMsRUFBRSxFQUFFO0lBQ2pMLGtCQUFrQjtJQUNsQixJQUFJLGVBQWUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtJQUMxRCxzQkFBc0I7SUFDdEIsZUFBZSxHQUFHLGVBQWUsR0FBRyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGVBQWUsQ0FBQTtJQUM5RixjQUFjO0lBQ2QsSUFBSSxNQUFNLEtBQUssU0FBUztRQUFFLFlBQVkscUJBQVEsWUFBWSxJQUFFLCtCQUErQixFQUFFLGVBQWUsR0FBRSxDQUFBO0lBQzlHLE1BQU0sT0FBTyxtQkFDWCw2QkFBNkIsRUFBRSxHQUFHLEVBQ2xDLGtDQUFrQyxFQUFFLElBQUksSUFDckMsWUFBWSxDQUNoQixDQUFBO0lBRUQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFBO0FBQzNELENBQUMsQ0FBQTtBQUVELHdCQUF3QjtBQUNYLFFBQUEsYUFBYSxHQUFHLENBQUMsUUFBYSxFQUFFLElBQVksRUFBRSxhQUFxQixHQUFHLEVBQUUsRUFBRSxDQUFDLG9CQUFZLENBQUMsUUFBUSxFQUFFLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUUsVUFBVSxDQUFDLENBQUE7QUFFaEssaUNBQWlDO0FBQ3BCLFFBQUEsaUJBQWlCLEdBQUcsQ0FBQyxRQUFrQixFQUFFLEVBQUUsQ0FBQyxRQUFRO0tBQzlELEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxHQUFHLENBQUM7S0FDdkMsR0FBRyxDQUFDLDhCQUE4QixFQUFFLFdBQVcsQ0FBQztLQUNoRCxHQUFHLENBQUMsOEJBQThCLEVBQUUsd0JBQXdCLENBQUM7S0FDN0QsR0FBRyxDQUFDLHdCQUF3QixFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVM7S0FDekQsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFBIn0=