/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_functions_1 = require("firebase-functions");
const server_1 = require("./server");
const server = server_1.default();
// Pass in your express app
exports.proxy = firebase_functions_1.https.onRequest(server);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7O0lBUUk7QUFDSixZQUFZLENBQUE7O0FBRVosMkRBQTBDO0FBQzFDLHFDQUFrQztBQUVsQyxNQUFNLE1BQU0sR0FBRyxnQkFBVyxFQUFFLENBQUE7QUFFNUIsMkJBQTJCO0FBQ2QsUUFBQSxLQUFLLEdBQUcsMEJBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUEifQ==