"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HelloService {
    static hello(name) {
        return "hello " + name;
    }
    static bye(name) {
        return "bye " + name;
    }
}
exports.default = HelloService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSGVsbG9TZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL3NlcnZpY2VzL0hlbGxvU2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLE1BQU0sWUFBWTtJQUNoQixNQUFNLENBQUMsS0FBSyxDQUFDLElBQVk7UUFDdkIsT0FBTyxRQUFRLEdBQUcsSUFBSSxDQUFBO0lBQ3hCLENBQUM7SUFFRCxNQUFNLENBQUMsR0FBRyxDQUFDLElBQVk7UUFDckIsT0FBTyxNQUFNLEdBQUcsSUFBSSxDQUFBO0lBQ3RCLENBQUM7Q0FDRjtBQUNELGtCQUFlLFlBQVksQ0FBQSJ9