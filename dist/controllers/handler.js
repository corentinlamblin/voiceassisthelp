"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Import HTTP def
const express_1 = require("express");
// Import Logger
const ApplicationLogger_1 = require("../config/ApplicationLogger");
// Import Response Helper
const response_1 = require("../utils/response");
// Import "process.env"
const process = require("process");
// Import helper
const util_1 = require("util");
/* import des responses dialogFlow */
// Import dialogFlow-fulfillment
const { WebhookClient } = require('dialogflow-fulfillment');
// Import analytics framework
const chatbase = require('@google/chatbase');
/**
 * Express Middleware to check for API Key and CORS pre-flight
 *
 * @param {Request} request Cloud Function request context.
 * @param {Response} response Cloud Function response context.
 * @param {NextFunction} next Process to the next middleware.
 */
exports.preprocessRequest = (request, response, next) => {
    // Handle CORS pre-flight
    if (request.method === `OPTIONS`)
        return response_1.preflightResponse(response);
    // API key check
    const apiKey = request.get('X-API-Key');
    if (!util_1.isNullOrUndefined(process.env.API_KEY)) {
        if (!apiKey)
            return response_1.errorResponse(response, 'Unauthorised access', 401);
        if (apiKey !== process.env.API_KEY)
            return response_1.errorResponse(response, 'Forbidden access', 403);
    }
    next();
};
exports.router = express_1.Router({ caseSensitive: true, strict: false });
/**
 * HTTP Cloud Function.
 *
 * @param {Object} request Cloud Function request context.
 * @param {Object} response Cloud Function response context.
 */
exports.router.post('', (request, response) => {
    ApplicationLogger_1.AppLogger.debug(`orderChat data: ${JSON.stringify(request.body)}`);
    let agent = new WebhookClient({ request: request, response: response });
    const qrytxt = request.body.queryResult.queryText;
    const intent = request.body.queryResult.intent.displayName;
    const APKEY = "AIzaSyBAPNalpqxdjZAPi4zShIOMvPJqEAVtbFM";
    chatbase
        .setApiKey(APKEY) // Your API key
        .setVersion('1.0') // The version of the bot deployed
        .setIntent(intent); // The intent of the user message
    if (agent.originalRequest.source !== undefined) {
        if (agent.originalRequest.source === "google") {
            chatbase
                .setUserId(agent.conv().user.id); // The id of the user from google
        }
        else {
            chatbase
                .setUserId('unknown'); // The id of the user from google
        }
        chatbase
            .setPlatform(agent.originalRequest.source); // The platfrom the bot is interacting on/over
    }
    else {
        chatbase
            .setUserId('unknown') // id not found
            .setPlatform(agent.UNSPECIFIED); // The platform not found
    }
    if (intent === "Default Fallback Intent") {
        chatbase.newMessage()
            .setTimestamp(`${new Date().getTime()}`)
            .setAsNotHandled()
            // Attach the input that the bot did not understand
            .setMessage(qrytxt)
            // Send it to the chatbase service
            .send()
            // Catch and print any errors when attempting to send
            .catch((e) => console.error(e));
    }
    else {
        chatbase.newMessage()
            .setTimestamp(`${new Date().getTime()}`)
            // Attach the input that the bot did not understand
            .setAsHandled()
            .setMessage(qrytxt)
            // Send it to the chatbase service
            .send()
            // Catch and print any errors when attempting to send
            .catch((e) => console.error(e));
    }
    launchAgent(request, response);
});
const launchAgent = (request, response) => {
    const actions = new Map([
        ["", action1],
        ["", action2],
        ["", action3]
    ]);
    const rightAction = Array.from(actions.keys()).filter(action => action.match(request.body.queryResult.action));
    const launch = actions.get(rightAction[0]);
    if (launch !== undefined)
        launch(request, response);
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGFuZGxlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9jb250cm9sbGVycy9oYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsa0JBQWtCO0FBQ2xCLHFDQUFpRTtBQUNqRSxnQkFBZ0I7QUFDaEIsbUVBQXVEO0FBQ3ZELHlCQUF5QjtBQUN6QixnREFBb0U7QUFDcEUsdUJBQXVCO0FBQ3ZCLG1DQUFrQztBQUNsQyxnQkFBZ0I7QUFDaEIsK0JBQXdDO0FBQ3hDLHFDQUFxQztBQUNyQyxnQ0FBZ0M7QUFDaEMsTUFBTSxFQUFFLGFBQWEsRUFBRSxHQUFHLE9BQU8sQ0FBRSx3QkFBd0IsQ0FBQyxDQUFBO0FBQzVELDZCQUE2QjtBQUM3QixNQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtBQUM1Qzs7Ozs7O0dBTUc7QUFDVSxRQUFBLGlCQUFpQixHQUFHLENBQUMsT0FBZ0IsRUFBRSxRQUFrQixFQUFFLElBQWtCLEVBQUUsRUFBRTtJQUM1Rix5QkFBeUI7SUFDekIsSUFBSSxPQUFPLENBQUMsTUFBTSxLQUFLLFNBQVM7UUFBRSxPQUFPLDRCQUFpQixDQUFDLFFBQVEsQ0FBQyxDQUFBO0lBQ3BFLGdCQUFnQjtJQUNoQixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0lBQ3ZDLElBQUksQ0FBQyx3QkFBaUIsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1FBQzNDLElBQUksQ0FBQyxNQUFNO1lBQUUsT0FBTyx3QkFBYSxDQUFDLFFBQVEsRUFBRSxxQkFBcUIsRUFBRSxHQUFHLENBQUMsQ0FBQTtRQUN2RSxJQUFJLE1BQU0sS0FBSyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU87WUFBRSxPQUFPLHdCQUFhLENBQUMsUUFBUSxFQUFFLGtCQUFrQixFQUFFLEdBQUcsQ0FBQyxDQUFBO0tBQzVGO0lBRUQsSUFBSSxFQUFFLENBQUE7QUFDUixDQUFDLENBQUE7QUFFWSxRQUFBLE1BQU0sR0FBVyxnQkFBTSxDQUFDLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFDLENBQUMsQ0FBQTtBQUUzRTs7Ozs7R0FLRztBQUNILGNBQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsT0FBZ0IsRUFBRSxRQUFrQixFQUFFLEVBQUU7SUFDdkQsNkJBQVMsQ0FBQyxLQUFLLENBQUMsbUJBQW1CLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUNsRSxJQUFJLEtBQUssR0FBRyxJQUFJLGFBQWEsQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBQyxDQUFDLENBQUE7SUFDdEUsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFBO0lBQ2pELE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUE7SUFFMUQsTUFBTSxLQUFLLEdBQUcseUNBQXlDLENBQUE7SUFDdkQsUUFBUTtTQUNMLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxlQUFlO1NBQ2hDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxrQ0FBa0M7U0FDcEQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsaUNBQWlDO0lBQ3ZELElBQUksS0FBSyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEtBQUssU0FBUyxFQUFFO1FBQzlDLElBQUksS0FBSyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEtBQUssUUFBUSxFQUFFO1lBQzdDLFFBQVE7aUJBQ0wsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUEsQ0FBQyxpQ0FBaUM7U0FDckU7YUFBTTtZQUNMLFFBQVE7aUJBQ0wsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFBLENBQUMsaUNBQWlDO1NBQzFEO1FBQ0QsUUFBUTthQUNMLFdBQVcsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFBLENBQUMsOENBQThDO0tBQzVGO1NBQU07UUFDTCxRQUFRO2FBQ0wsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLGVBQWU7YUFDcEMsV0FBVyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQSxDQUFDLHlCQUF5QjtLQUM1RDtJQUVELElBQUksTUFBTSxLQUFLLHlCQUF5QixFQUFFO1FBQ3hDLFFBQVEsQ0FBQyxVQUFVLEVBQUU7YUFDbEIsWUFBWSxDQUFDLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDO2FBQ3ZDLGVBQWUsRUFBRTtZQUNsQixtREFBbUQ7YUFDbEQsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUNuQixrQ0FBa0M7YUFDakMsSUFBSSxFQUFFO1lBQ1AscURBQXFEO2FBQ3BELEtBQUssQ0FBQyxDQUFDLENBQVEsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO0tBQ3pDO1NBQU07UUFDTCxRQUFRLENBQUMsVUFBVSxFQUFFO2FBQ2xCLFlBQVksQ0FBQyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQztZQUN4QyxtREFBbUQ7YUFDbEQsWUFBWSxFQUFFO2FBRWQsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUNuQixrQ0FBa0M7YUFDakMsSUFBSSxFQUFFO1lBQ1AscURBQXFEO2FBQ3BELEtBQUssQ0FBQyxDQUFDLENBQVEsRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO0tBQ3pDO0lBRUQsV0FBVyxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQTtBQUVoQyxDQUFDLENBQUMsQ0FBQTtBQUVGLE1BQU0sV0FBVyxHQUFHLENBQUMsT0FBZ0IsRUFBRSxRQUFrQixFQUFFLEVBQUU7SUFDM0QsTUFBTSxPQUFPLEdBQUcsSUFBSSxHQUFHLENBQ3JCO1FBQ0UsQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDO1FBQ2IsQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDO1FBQ2IsQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDO0tBQ2QsQ0FDRixDQUFBO0lBQ0QsTUFBTSxXQUFXLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQ25ELE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFBO0lBRTFELE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDMUMsSUFBSSxNQUFNLEtBQUssU0FBUztRQUN0QixNQUFNLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFBO0FBQzdCLENBQUMsQ0FBQSJ9