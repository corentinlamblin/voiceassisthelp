"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HelloService_1 = require("../services/HelloService");
const express_1 = require("express");
const response_1 = require("../utils/response");
const util_1 = require("util");
const assert = require("assert");
/**
 * Express Middleware to check for API Key and CORS pre-flight
 *
 * @param {Request} request Cloud Function request context.
 * @param {Response} response Cloud Function response context.
 * @param {NextFunction} next Process to the next middleware.
 */
exports.preprocessRequest = (request, response, next) => {
    // Handle CORS pre-flight
    if (request.method === `OPTIONS`)
        return response_1.preflightResponse(response);
    // API Key check
    const apiKey = request.get('X-API-Key');
    if (!util_1.isNullOrUndefined(process.env.API_KEY)) {
        if (!apiKey)
            return response_1.errorResponse(response, 'Unauthorised access', 401);
        if (apiKey !== process.env.API_KEY)
            return response_1.errorResponse(response, 'Forbidden access', 403);
    }
    next();
};
// Assign router to the express.Router() instance
exports.router = express_1.Router({ caseSensitive: true, strict: false });
assert.notEqual(exports.router, undefined, "router undefined");
exports.router.get('/hello', (request, response) => {
    request.query.name !== undefined ? response.status(200).send(HelloService_1.default.hello(request.query.name)).end() : response.status(200).send("Hello World").end();
});
exports.router.get('/bye', (request, response) => {
    request.query.name !== undefined ? response.status(200).send(HelloService_1.default.bye(request.query.name)).end() : response.status(200).send("Bye World").end();
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVsbG9Db250cm9sbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbnRyb2xsZXJzL2hlbGxvQ29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLDJEQUFtRDtBQUNuRCxxQ0FBaUU7QUFDakUsZ0RBQW9FO0FBQ3BFLCtCQUF3QztBQUN4QyxpQ0FBaUM7QUFHakM7Ozs7OztHQU1HO0FBQ1UsUUFBQSxpQkFBaUIsR0FBRyxDQUFDLE9BQWdCLEVBQUUsUUFBa0IsRUFBRSxJQUFrQixFQUFFLEVBQUU7SUFDNUYseUJBQXlCO0lBQ3pCLElBQUksT0FBTyxDQUFDLE1BQU0sS0FBSyxTQUFTO1FBQUUsT0FBTyw0QkFBaUIsQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUVwRSxnQkFBZ0I7SUFDaEIsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTtJQUN2QyxJQUFJLENBQUMsd0JBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRTtRQUMzQyxJQUFJLENBQUMsTUFBTTtZQUFFLE9BQU8sd0JBQWEsQ0FBQyxRQUFRLEVBQUUscUJBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFDdkUsSUFBSSxNQUFNLEtBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPO1lBQUUsT0FBTyx3QkFBYSxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsRUFBRSxHQUFHLENBQUMsQ0FBQTtLQUM1RjtJQUNELElBQUksRUFBRSxDQUFBO0FBQ1IsQ0FBQyxDQUFBO0FBRUQsaURBQWlEO0FBQ3BDLFFBQUEsTUFBTSxHQUFXLGdCQUFNLENBQUMsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFBO0FBRTVFLE1BQU0sQ0FBQyxRQUFRLENBQUMsY0FBTSxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFBO0FBRXRELGNBQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBZ0IsRUFBRSxRQUFrQixFQUFFLEVBQUU7SUFDNUQsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBWSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFBO0FBQzdKLENBQUMsQ0FBQyxDQUFBO0FBQ0YsY0FBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxPQUFnQixFQUFFLFFBQWtCLEVBQUUsRUFBRTtJQUMxRCxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLHNCQUFZLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUE7QUFDekosQ0FBQyxDQUFDLENBQUEifQ==