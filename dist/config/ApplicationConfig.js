"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nconf = require("nconf");
const path = require("path");
class ApplicationConfig extends nconf.Provider {
    /**
     * Class constructor for configuration wrapper class
     */
    constructor() {
        super();
        // Set config.json configuration file location
        this.configFile = process.env.APP_CONFIG_FILE || path.resolve("." + path.sep, "config.json");
        this.loadConfig();
        this.defaultValues();
    }
    static get Instance() {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new ApplicationConfig();
        }
        return this.instance;
    }
    /**
     * Load configuration files
     */
    loadConfig() {
        this.argv()
            .env({
            separator: "_",
            match: /^app/
        })
            .file({ file: this.configFile });
    }
    /**
     *  Initialize default value for missing configuration key
     */
    defaultValues() {
        this.defaults({
            "app": {
                "http": {
                    "listen": "0.0.0.0",
                    "port": 3000
                }
            }
        });
    }
}
exports.default = ApplicationConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBwbGljYXRpb25Db25maWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY29uZmlnL0FwcGxpY2F0aW9uQ29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsK0JBQThCO0FBQzlCLDZCQUE0QjtBQUU1QixNQUFNLGlCQUFrQixTQUFRLEtBQUssQ0FBQyxRQUFRO0lBWTVDOztPQUVHO0lBQ0g7UUFDRSxLQUFLLEVBQUUsQ0FBQTtRQWZULDhDQUE4QztRQUN0QyxlQUFVLEdBQVcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxhQUFhLENBQUMsQ0FBQTtRQWVyRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7UUFDakIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBO0lBQ3RCLENBQUM7SUFiRCxNQUFNLEtBQUssUUFBUTtRQUNqQixJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQ3pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFBO1NBQ3hDO1FBQ0QsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFBO0lBQ3RCLENBQUM7SUFVRDs7T0FFRztJQUNLLFVBQVU7UUFDaEIsSUFBSSxDQUFDLElBQUksRUFBRTthQUNSLEdBQUcsQ0FBQztZQUNILFNBQVMsRUFBRSxHQUFHO1lBQ2QsS0FBSyxFQUFFLE1BQU07U0FDZCxDQUFDO2FBQ0QsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFBO0lBQ3BDLENBQUM7SUFFRDs7T0FFRztJQUNLLGFBQWE7UUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FDWDtZQUNFLEtBQUssRUFBRTtnQkFDTCxNQUFNLEVBQUU7b0JBQ04sUUFBUSxFQUFFLFNBQVM7b0JBQ25CLE1BQU0sRUFBRSxJQUFJO2lCQUNiO2FBQ0Y7U0FDRixDQUNGLENBQUE7SUFDSCxDQUFDO0NBQ0Y7QUFFRCxrQkFBZSxpQkFBaUIsQ0FBQSJ9