"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ApplicationLogger_1 = require("./ApplicationLogger");
class StreamLogger {
    write(message) {
        ApplicationLogger_1.AppLogger.info(message.slice(0, -1)); // trick to supress extra LF
    }
}
exports.StreamLogger = StreamLogger;
exports.default = StreamLogger;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9nZ2VyU3RyZWFtLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbmZpZy9Mb2dnZXJTdHJlYW0udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwyREFBK0M7QUFFL0MsTUFBYSxZQUFZO0lBQ3ZCLEtBQUssQ0FBRSxPQUFlO1FBQ3BCLDZCQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDLDRCQUE0QjtJQUNuRSxDQUFDO0NBQ0Y7QUFKRCxvQ0FJQztBQUVELGtCQUFlLFlBQVksQ0FBQSJ9