"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const winston_1 = require("winston");
// Load Application config
const ApplicationConfig_1 = require("./ApplicationConfig");
class ApplicationLogger extends winston_1.Logger {
    constructor() {
        super({ transports: [
                new winston_1.transports.File({
                    level: ApplicationConfig_1.default.Instance.get("app:log:level"),
                    filename: ApplicationConfig_1.default.Instance.get("app:log:filename"),
                    json: true,
                    maxsize: 5242880,
                    maxFiles: 5,
                    colorize: false
                }),
                new winston_1.transports.Console({
                    level: ApplicationConfig_1.default.Instance.get("app:log:level"),
                    handleExceptions: true,
                    json: false,
                    colorize: true
                })
            ] });
        this.logDirectory = ApplicationConfig_1.default.Instance.get("app:log:filename") || path.resolve(".");
    }
}
exports.default = ApplicationLogger;
exports.AppLogger = new ApplicationLogger();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBwbGljYXRpb25Mb2dnZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY29uZmlnL0FwcGxpY2F0aW9uTG9nZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsNkJBQTRCO0FBQzVCLHFDQUE0QztBQUM1QywwQkFBMEI7QUFDMUIsMkRBQW1EO0FBRW5ELE1BQXFCLGlCQUFrQixTQUFRLGdCQUFNO0lBR25EO1FBQ0UsS0FBSyxDQUFDLEVBQUUsVUFBVSxFQUFFO2dCQUNsQixJQUFJLG9CQUFVLENBQUMsSUFBSSxDQUFDO29CQUNsQixLQUFLLEVBQUUsMkJBQWlCLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUM7b0JBQ3RELFFBQVEsRUFBRSwyQkFBaUIsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDO29CQUM1RCxJQUFJLEVBQUUsSUFBSTtvQkFDVixPQUFPLEVBQUUsT0FBTztvQkFDaEIsUUFBUSxFQUFFLENBQUM7b0JBQ1gsUUFBUSxFQUFFLEtBQUs7aUJBQ2hCLENBQUM7Z0JBQ0YsSUFBSSxvQkFBVSxDQUFDLE9BQU8sQ0FBQztvQkFDckIsS0FBSyxFQUFFLDJCQUFpQixDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDO29CQUN0RCxnQkFBZ0IsRUFBRSxJQUFJO29CQUN0QixJQUFJLEVBQUUsS0FBSztvQkFDWCxRQUFRLEVBQUUsSUFBSTtpQkFDZixDQUFDO2FBQ0gsRUFBQyxDQUFDLENBQUE7UUFsQkksaUJBQVksR0FBVywyQkFBaUIsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQTtJQW1CdkcsQ0FBQztDQUNGO0FBckJELG9DQXFCQztBQUVVLFFBQUEsU0FBUyxHQUFHLElBQUksaUJBQWlCLEVBQUUsQ0FBQSJ9