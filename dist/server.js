/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const LoggerStream_1 = require("./config/LoggerStream");
// Backend
//import { router, preprocessRequest } from './controllers/helloController' if you want to test HelloWorld REST API as a bird in the cloud
const handler_1 = require("./controllers/handler");
const assert = require("assert");
// Constants
const projectId = process.env.GCLOUD_PROJECT;
const region = process.env.FUNCTION_REGION;
const context = "proxy";
const path = "/voice";
const endpoint = (process.env.GCLOUD_PROJECT) ? `${path}` : `/${context}${path}`;
const corsOptions = {
    "origin": "*",
    "methods": "GET,PUT,POST,DELETE",
    "maxAge": 3600,
    "credentials": true,
    "optionsSuccesStatus": 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
const setupServer = () => {
    const app = express();
    assert.notEqual(handler_1.router, undefined, "router undefined");
    // Add access logs
    app.use(morgan("combined", { stream: new LoggerStream_1.StreamLogger() }));
    // Handle CORS
    app.use(cors(corsOptions));
    // Parse application/json body
    app.use(bodyParser.json());
    // API key and CORS preflight check
    app.use(endpoint, handler_1.preprocessRequest);
    // INIT express Router
    app.use(endpoint, handler_1.router);
    return app;
};
exports.default = setupServer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL3NlcnZlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7SUFRSTtBQUNKLFlBQVksQ0FBQTs7QUFFWiw2QkFBOEI7QUFDOUIsbUNBQW9DO0FBQ3BDLDBDQUF5QztBQUN6QyxpQ0FBa0M7QUFDbEMsd0RBQW9EO0FBQ3BELFVBQVU7QUFDViwwSUFBMEk7QUFDMUksbURBQWlFO0FBQ2pFLGlDQUFnQztBQUVoQyxZQUFZO0FBQ1osTUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUE7QUFDNUMsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUE7QUFDMUMsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFBO0FBQ3ZCLE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQTtBQUNyQixNQUFNLFFBQVEsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxHQUFHLElBQUksRUFBRSxDQUFBO0FBQ2hGLE1BQU0sV0FBVyxHQUFHO0lBQ2xCLFFBQVEsRUFBRSxHQUFHO0lBQ2IsU0FBUyxFQUFFLHFCQUFxQjtJQUNoQyxRQUFRLEVBQUUsSUFBSTtJQUNkLGFBQWEsRUFBRSxJQUFJO0lBQ25CLHFCQUFxQixFQUFFLEdBQUcsQ0FBQyw2REFBNkQ7Q0FDekYsQ0FBQTtBQUVELE1BQU0sV0FBVyxHQUFHLEdBQUcsRUFBRTtJQUV2QixNQUFNLEdBQUcsR0FBRSxPQUFPLEVBQUUsQ0FBQTtJQUNwQixNQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLEVBQUUsU0FBUyxFQUFFLGtCQUFrQixDQUFDLENBQUE7SUFFdEQsa0JBQWtCO0lBQ2xCLEdBQUcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLDJCQUFZLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQTtJQUUxRCxjQUFjO0lBQ2QsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQTtJQUUxQiw4QkFBOEI7SUFDOUIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQTtJQUUxQixtQ0FBbUM7SUFDbkMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsMkJBQWlCLENBQUMsQ0FBQTtJQUVwQyxzQkFBc0I7SUFDdEIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsZ0JBQU0sQ0FBQyxDQUFBO0lBRXpCLE9BQU8sR0FBRyxDQUFBO0FBQ1osQ0FBQyxDQUFBO0FBRUQsa0JBQWUsV0FBVyxDQUFBIn0=