constwebpack = require("webpack");
constpath = require('path');

module.exports = {
  devtool: 'inline-source-map',
  entry: './src/main.ts',
  target: 'node',
  output: {
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].map',
    path: path.join(__dirname, 'dist'),
    libraryTarget: "commonjs",
  },
  resolve: {
    // Add '.ts' and '.tsx' as a resolvable extension.
    extensions: [".webpack.js", ".ts", ".tsx", ".js"],
    modules: [path.resolve('./src'), 'node_modules']
  },
  module: {
    rules:
      // all files with a '.ts' or '.tsx' extension will be handled by'ts-loader'
      [{
      loader: "ts-loader",
      test: /\.tsx?$/,
      exclude: path.resolve('./node_modules')
    }]
  }
}