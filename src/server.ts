/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict'

import cors = require('cors');
import express = require('express');
import * as bodyParser from 'body-parser'
import morgan = require('morgan');
import { StreamLogger } from './config/LoggerStream'
// Backend
//import { router, preprocessRequest } from './controllers/helloController' if you want to test HelloWorld REST API as a bird in the cloud
import { router, preprocessRequest } from './controllers/handler'
import * as assert from "assert"

// Constants
const projectId = process.env.GCLOUD_PROJECT
const region = process.env.FUNCTION_REGION
const context = "proxy"
const path = "/voice"
const endpoint = (process.env.GCLOUD_PROJECT) ? `${path}` : `/${context}${path}`
const corsOptions = {
  "origin": "*",
  "methods": "GET,PUT,POST,DELETE",
  "maxAge": 3600,
  "credentials": true,
  "optionsSuccesStatus": 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

const setupServer = () => {

  const app= express()
  assert.notEqual(router, undefined, "router undefined")

  // Add access logs
  app.use(morgan("combined", {stream: new StreamLogger() }))

  // Handle CORS
  app.use(cors(corsOptions))

  // Parse application/json body
  app.use(bodyParser.json())

  // API key and CORS preflight check
  app.use(endpoint, preprocessRequest)

  // INIT express Router
  app.use(endpoint, router)

  return app
}

export default setupServer