/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict'

import { https } from 'firebase-functions'
import setupServer from './server'

const server = setupServer()

// Pass in your express app
export const proxy = https.onRequest(server)
