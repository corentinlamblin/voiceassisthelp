import HelloService from "../services/HelloService"
import { Request, Response, NextFunction, Router } from 'express'
import { preflightResponse, errorResponse } from "../utils/response"
import { isNullOrUndefined } from "util"
import *  as assert from "assert"


/**
 * Express Middleware to check for API Key and CORS pre-flight
 *
 * @param {Request} request Cloud Function request context.
 * @param {Response} response Cloud Function response context.
 * @param {NextFunction} next Process to the next middleware.
 */
export const preprocessRequest = (request: Request, response: Response, next: NextFunction) => {
  // Handle CORS pre-flight
  if (request.method === `OPTIONS`) return preflightResponse(response)

  // API Key check
  const apiKey = request.get('X-API-Key')
  if (!isNullOrUndefined(process.env.API_KEY)) {
    if (!apiKey) return errorResponse(response, 'Unauthorised access', 401)
    if (apiKey !== process.env.API_KEY) return errorResponse(response, 'Forbidden access', 403)
  }
  next()
}

// Assign router to the express.Router() instance
export const router: Router = Router({ caseSensitive: true, strict: false })

assert.notEqual(router, undefined, "router undefined")

router.get('/hello', (request: Request, response: Response) => {
  request.query.name !== undefined ? response.status(200).send(HelloService.hello(request.query.name)).end() : response.status(200).send("Hello World").end()
})
router.get('/bye', (request: Request, response: Response) => {
  request.query.name !== undefined ? response.status(200).send(HelloService.bye(request.query.name)).end() : response.status(200).send("Bye World").end()
})