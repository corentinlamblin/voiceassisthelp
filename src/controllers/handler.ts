// Import HTTP def
import { Request, Response, NextFunction, Router } from 'express'
// Import Logger
import { AppLogger } from '../config/ApplicationLogger'
// Import Response Helper
import { errorResponse, preflightResponse } from '../utils/response'
// Import "process.env"
import * as process from 'process'
// Import helper
import { isNullOrUndefined } from 'util'
/* import des responses dialogFlow */
// Import dialogFlow-fulfillment
const { WebhookClient } = require ('dialogflow-fulfillment')
// Import analytics framework
const chatbase = require('@google/chatbase')
/**
 * Express Middleware to check for API Key and CORS pre-flight
 *
 * @param {Request} request Cloud Function request context.
 * @param {Response} response Cloud Function response context.
 * @param {NextFunction} next Process to the next middleware.
 */
export const preprocessRequest = (request: Request, response: Response, next: NextFunction) => {
  // Handle CORS pre-flight
  if (request.method === `OPTIONS`) return preflightResponse(response)
  // API key check
  const apiKey = request.get('X-API-Key')
  if (!isNullOrUndefined(process.env.API_KEY)) {
    if (!apiKey) return errorResponse(response, 'Unauthorised access', 401)
    if (apiKey !== process.env.API_KEY) return errorResponse(response, 'Forbidden access', 403)
  }

  next()
}

export const router: Router = Router({ caseSensitive: true, strict: false})

/**
 * HTTP Cloud Function.
 *
 * @param {Object} request Cloud Function request context.
 * @param {Object} response Cloud Function response context.
 */
router.post('', (request: Request, response: Response) => {
  AppLogger.debug(`orderChat data: ${JSON.stringify(request.body)}`)
  let agent = new WebhookClient({ request: request, response: response})
  const qrytxt = request.body.queryResult.queryText
  const intent = request.body.queryResult.intent.displayName

  const APKEY = "AIzaSyBAPNalpqxdjZAPi4zShIOMvPJqEAVtbFM"
  chatbase
    .setApiKey(APKEY) // Your API key
    .setVersion('1.0') // The version of the bot deployed
    .setIntent(intent); // The intent of the user message
  if (agent.originalRequest.source !== undefined) {
    if (agent.originalRequest.source === "google") {
      chatbase
        .setUserId(agent.conv().user.id) // The id of the user from google
    } else {
      chatbase
        .setUserId('unknown') // The id of the user from google
    }
    chatbase
      .setPlatform(agent.originalRequest.source) // The platfrom the bot is interacting on/over
  } else {
    chatbase
      .setUserId('unknown') // id not found
      .setPlatform(agent.UNSPECIFIED) // The platform not found
  }

  if (intent === "Default Fallback Intent") {
    chatbase.newMessage()
      .setTimestamp(`${new Date().getTime()}`)
      .setAsNotHandled()
      // Attach the input that the bot did not understand
      .setMessage(qrytxt)
      // Send it to the chatbase service
      .send()
      // Catch and print any errors when attempting to send
      .catch((e: Error) => console.error(e))
  } else {
    chatbase.newMessage()
      .setTimestamp(`${new Date().getTime()}`)
      // Attach the input that the bot did not understand
      .setAsHandled()

      .setMessage(qrytxt)
      // Send it to the chatbase service
      .send()
      // Catch and print any errors when attempting to send
      .catch((e: Error) => console.error(e))
  }

  launchAgent(request, response)

})

const launchAgent = (request: Request, response: Response) => {
  const actions = new Map(
    [
      ["", action1],
      ["", action2],
      ["", action3]
    ]
  )
  const rightAction = Array.from(actions.keys()).filter(
    action => action.match(request.body.queryResult.action))

  const launch = actions.get(rightAction[0])
  if (launch !== undefined)
    launch(request, response)
}