import * as nconf from "nconf"
import * as path from "path"

class ApplicationConfig extends nconf.Provider {
  // Set config.json configuration file location
  private configFile: string = process.env.APP_CONFIG_FILE || path.resolve("." + path.sep, "config.json")

  private static instance: ApplicationConfig

  static get Instance() {
    if (this.instance === null || this.instance === undefined) {
      this.instance = new ApplicationConfig()
    }
    return this.instance
  }
  /**
   * Class constructor for configuration wrapper class
   */
  private constructor() {
    super()
    this.loadConfig()
    this.defaultValues()
  }

  /**
   * Load configuration files
   */
  private loadConfig() {
    this.argv()
      .env({
        separator: "_",
        match: /^app/
      })
      .file({ file: this.configFile })
  }

  /**
   *  Initialize default value for missing configuration key
   */
  private defaultValues() {
    this.defaults(
      {
        "app": {
          "http": {
            "listen": "0.0.0.0",
            "port": 3000
          }
        }
      }
    )
  }
}

export default ApplicationConfig