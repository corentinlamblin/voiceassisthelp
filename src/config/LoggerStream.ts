import { AppLogger } from "./ApplicationLogger"

export class StreamLogger {
  write (message: String) {
    AppLogger.info(message.slice(0, -1)) // trick to supress extra LF
  }
}

export default StreamLogger