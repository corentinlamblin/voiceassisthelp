import * as Path from "path"
import * as path from "path"
import { transports, Logger } from "winston"
// Load Application config
import ApplicationConfig from "./ApplicationConfig"

export default class ApplicationLogger extends Logger {
  readonly logDirectory: String = ApplicationConfig.Instance.get("app:log:filename") || path.resolve(".")

  public constructor() {
    super({ transports: [
      new transports.File({
        level: ApplicationConfig.Instance.get("app:log:level"),
        filename: ApplicationConfig.Instance.get("app:log:filename"),
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false
      }),
      new transports.Console({
        level: ApplicationConfig.Instance.get("app:log:level"),
        handleExceptions: true,
        json: false,
        colorize: true
      })
    ]})
  }
}

export let AppLogger = new ApplicationLogger()