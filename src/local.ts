/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict'

// Setup Express for Google Cloud Functions
import setupServer from './server'

const server = setupServer()

// Start local express app (for debug purpose)
const port = Number(process.env.PORT) || 9000

server.listen(port, () => console.log(`🚀  Server ready at port ${port}`))