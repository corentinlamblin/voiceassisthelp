class HelloService {
  static hello(name: string): string {
    return "hello " + name
  }

  static bye(name: string): string  {
    return "bye " + name
  }
}
export default HelloService