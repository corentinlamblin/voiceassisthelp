/**
  * {{{
  *  _____                        www.leansys.fr
  * |     |_.-----.---.-.-----.-----.--.--.-----.
  * |       |  -__|  _  |     |__ --|  |  |__ --|
  * |_______|_____|___._|__|__|_____|___  |_____|
  *                                 |_____|
  * }}}
  */
'use strict'

// Import HTTP def
import { Response } from 'express'

const RESPONSE_ALLOWED_HEADERS = 'Content-Type, Content-Length, Accept, Origin, ETag, Authorization, X-Requested-With, X-CSRF-Token, X-Api-Key'

// Callback response helper
export const sendResponse = (response: Response, body: Object | string | undefined, statusCode: number = 200, extraHeaders: Object = {}, cookie: String | undefined = undefined) => {
  // Get header keys
  let extraHeaderKeys = Object.keys(extraHeaders).join(", ")
  // Add security header
  extraHeaderKeys = extraHeaderKeys + (extraHeaderKeys.length > 0 ? ", " : "") + "Authorization"
  // Add cookies
  if (cookie !== undefined) extraHeaders = { ...extraHeaders, "Access-Control-Expose-Headers": extraHeaderKeys }
  const headers = {
    "Access-Control-Allow-Origin": "*", // Required for CORS support to work
    "Access-Control-Allow-Credentials": true, // Required for cookies, authorization headers with HTTPS
    ...extraHeaders
  }

  response.set(headers).status(statusCode).send(body).end()
}

// Error response helper
export const errorResponse = (response: any, body: String, statusCode: number = 500) => sendResponse(response, { "code": "Error", "message": body }, statusCode)

// CORS preflight response helper
export const preflightResponse = (response: Response) => response
  .set('Access-Control-Allow-Origin', '*')
  .set('Access-Control-Allow-Methods', 'GET, POST')
  .set('Access-Control-Allow-Headers', RESPONSE_ALLOWED_HEADERS)
  .set('Access-Control-Max-Age', `${1 * 60 * 60}`) // 1 hour
  .status(204).send().end()